import { LitElement, html, css } from 'lit-element';

class PersonaBootStrap extends LitElement {

    static get properties() {
        return {
        };
    }
    static get styles() {
        return css`
            .redbg {
                background-color: red;
            }

            .greenbg {
                background-color: green;
            }

            .bluebg {
                background-color: blue;
            }

            .greybh {
                background-color: grey;
            }
        `;
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h1>Test Bootstratp</h1>
            <div class="row greybg">
                <div class="col redbg">Col 1</div>
                <div class="col greenbg">Col 2</div>
                <div class="col bluebg">Col 3</div>
            </div>
        `;
    }
}

customElements.define('persona-bootstrap', PersonaBootStrap)