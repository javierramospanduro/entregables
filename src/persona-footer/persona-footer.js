import { LitElement, html, css } from 'lit-element';
const miEmail="djavierramos@hotmailcom";

class PersonaFooter extends LitElement {

    static get styles() {
        return css`
            .advice{
                align:"center";                
            }
            .separador{
                size:8px;
                color:black;
            }
        
        `;
    }
    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <hr class=separador>
            <h5 class=advice >@Persona App 2020 by Javier Ramos Panduro</h5>
            <h5> <a href="mailto:"+miEmail>Contacto</a> </h5>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter)