import { LitElement, html } from 'lit-element'; 
import '../persona-header/persona-header.js'; 
import '../persona-main/persona-main.js'; 
import '../persona-footer/persona-footer.js'; 
import '../persona-sidebar/persona-sidebar.js';  
import '../persona-bootstrap/persona-bootstrap.js'
import '../persona-stats/persona-stats.js'

   
class PersonaApp extends LitElement {
	static get properties() {
		return {			
		};
	}

	constructor() {
		super();			
	}

	render() {
		return html`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
			</persona-header>
			<div class="row">
				<persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
				<persona-main class="col-10" @update-people="${this.updatePerson}"></persona-main>
				<persona-stats @gather-people="${this.updateStats}"></persona-stats>
			</div>			
			<persona-footer></persona-footer>			
		`;
	}    
	updateStats(e) {
		console.log("persona-app - update stats");
		console.log("persona-app - mi people stats " + e.detail.peopleStats);
		this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
	}
	updatePerson(e) {
		console.log("persona-app - update person");		
		console.log("persona app - mi people es " + JSON.stringify(e.detail.people));	
		this.shadowRoot.querySelector("persona-stats").people = e.detail.people;
	}
	newPerson(e) {	
		console.log("persona-app - new person");	
		this.shadowRoot.querySelector("persona-main").showPersonForm = true; 	  	
	}
}
customElements.define('persona-app', PersonaApp)  