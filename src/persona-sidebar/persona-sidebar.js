import { LitElement, html } from 'lit-element';  
	class PersonaSidebar extends LitElement {
	static get properties() {
		return {		
			peopleStats: {type:Object}	
		};
	}

	constructor() {
		super();			
		this.peopleStats = {numberOfPeople:0};
	}

	render() {
		return html`		
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
			<aside>
				<section>
					<div>Hay <span class="bagde bg-pill bg-primary">${this.peopleStats.numberOfPeople} </span> personas</div>				
					<div class="mt-5">
						<button class="w-100 btn bg-success" style="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
					<div>				
				</section>
			</aside>
		`;
	}    

	newPerson(e) {
		console.log("newPerson en persona-sidebar");
		console.log("Se va a crear una nueva persona");
  
		this.dispatchEvent(new CustomEvent("new-person", {})); 	
	}
	updated(changedProperties) {
		console.log("persona-sidebar - changedprops");
		if (changedProperties.has("peopleStats")) {
			console.log("persona-sidebar - se cambio el peopleStats");
		}
	}

}
customElements.define('persona-sidebar', PersonaSidebar)