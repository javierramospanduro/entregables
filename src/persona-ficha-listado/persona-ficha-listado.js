import { LitElement, html, css } from 'lit-element';  
	class PersonaFichaListado extends LitElement { 
	
	static get properties() {
		// Contiene los datos de la persona y la url a la foto
		// Si la url (que se recupera via API, no está cargada)
		// se indicará mediante el boolean isLoaded
		return {
			name: {type: String},
			yearsInCompany: {type: Number},
			profile: {type: String},
			photo: {type: String},
			isLoaded: {type: Boolean}
		};
	}

	static get styles() {
		// Propiedades del spinner para la imagen
		return css`
			.loader {
				border: 16px solid #f3f3f3; /* Light grey */
				border-top: 16px solid #3498db; /* Blue */
				border-radius: 50%;
				width: 120px;
				height: 120px;
				animation: spin 2s linear infinite;
			}

				@keyframes spin {
				0% { transform: rotate(0deg); }
				100% { transform: rotate(360deg); }
			}
		`;
	}

	constructor() {
		super();			
	}


	render() {		
		// Si la imagen no viene cargada la recuperaremos mediante la API
		console.log("Mi propiedad es: " + this.isLoaded);
		if (this.isLoaded === false) {
			console.log("La image no estaba cargada, cargamos");
			this.loadImage();
		}
		return html`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
			<div class="card h-100">
					<!-- Si no está cargada la imagen mostraremos un spinner -->
				<div class="card-header">${this.name}</div>
				<div class="card-body">
					${this.isLoaded === true ? html`<img src="${this.photo}" height="120" width="120" class="card-img-top" alt="${this.name}">` : 
						html`<div class="loader"></div>`}
					<p class="card-text">${this.profile}</p>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
					</ul>
				</div>
			<!-- Este botón nos permite eliminar la persona del array-->				
			<div class="card-footer">
				<button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>X</strong></button>
			</div>				
		</div>
	`;
}

	deletePerson(e) {	
		this.dispatchEvent(
			new CustomEvent("delete-person", {
					detail: {
						name: this.name
					}
				}
			)
		);
	}

	updated(changedProps) {
		console.log("Me cambian propiedades");
		if (this.isLoaded === false && changedProps.isLoaded === true ) {
			console.log("Me han recargado la imagen");
			this.render();
		}
	}
	prepareData(data) {
		console.log("Imagen cargada");
		this.photo = data.message;
		this.isLoaded = true;
	}

	loadImage() {
		console.log("Cargando imagen");
		fetch('https://dog.ceo/api/breeds/image/random')
			.then(response => response.json())
			.then(data => this.prepareData(data));
	}

}  
customElements.define('persona-ficha-listado', PersonaFichaListado)