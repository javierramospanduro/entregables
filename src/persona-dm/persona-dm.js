import { LitElement, html } from 'lit-element';

class PersonaDM extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet.",
                isLoaded: false
            }, {
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile: "Lorem ipsum.",
                isLoaded: false
            }, {
                name: "Éowyn",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
                isLoaded: false
            }, {
                name: "Turanga Leela",
                yearsInCompany: 9,
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit.",
                isLoaded: false
            }, {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                profile: "Lorem ipsum.",
                isLoaded: false
            }
        ];

    }

    updated(changedProperties) {
        console.log("persona-dm changed properties");

        if(changedProperties.has("people")) {
            console.log("persona-dm, Ha cambiado el valor de la prpiedad people");

            this.dispatchEvent(
                new CustomEvent(
                    "people-data-updated",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )
        }
    }
}

customElements.define('persona-dm', PersonaDM)