import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-dm/persona-dm.js';

const numeroDeFilas=5;

class PersonaMain extends LitElement {
	static get properties() {
		return {
			people: { type: Array },
			showPersonForm: { type: Boolean }
		};
	}

	constructor() {
		// Cargamos manualmente una lista de personas. La imagen se recuperará de una API
		// Para saber si la imagen se ha recuperado (la API es asíncrona) tenemos un boolean que nos indica si 
		// la imagen se ha cargado o no, para que el componente pueda mostrar un spinner en su lugar
		// Se initializa a false
		super();
		this.people = [];
		// La APP dispone de un form para insertar una nueva persona, este form no está visible, y se muestra con un button en el sidebar
		// Este boolean controla si se muestra o no
		this.showPersonForm = false;
	}

	render() {
		return html`
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<h2 class="text-center">Personas</h2>
		<div class="row" id="peopleList">			
			<div class="row row-cols-1 row-cols-sm-4">
				<!-- Por cada item de la lista en inyectamos una ficha en el DOM -->
				${this.people.map(
					person =>
						html`<persona-ficha-listado 
									name="${person.name}" 
									yearsInCompany="${person.yearsInCompany}" 
									profile="${person.profile}" 
									photo="${person.photo}"
									.isLoaded="${person.isLoaded}"
									@delete-person="${this.deletePerson}">
								</persona-ficha-listado>`
				)}
			</div>
			<persona-dm @people-data-updated="${this.peopleDataUpdated}"></persona-dm>
		</div>
		<!-- Solo pintaremos el formulario cuando el boolean este activo, esto se gestiona mediante evento en el button del sidebar -->
        ${this.showPersonForm ?
				html`
                <div class="row">
                    <persona-form id="personForm" class="d-none border rounded border-primary"
                        @persona-form-close="${this.personFormClose}"
                        @persona-form-store="${this.personFormStore}" >
                    </persona-form>
                </div>
                ` : html`<div></div>`
			}
	`;
	}

	updated(changedProperties) {
		console.log("Se han cambiado las propiedades")
		// Gestiona el mostrar el formulario de alta al cambiar la propiedad showPersonForm
		if (changedProperties.has("showPersonForm")) {
			if (this.showPersonForm === true) {
				this.showPersonFormData();
			} else {
				this.showPersonList();
			}
		}
		if (changedProperties.has("people")) {
			console.log("persona-main, changed people -> " + JSON.stringify(this.people));
			this.dispatchEvent(new CustomEvent("update-people",{detail:{people:this.people}}));
		}
	}

	deletePerson(e) {
		// Elimina una persona del array
		this.people = this.people.filter(
			person => person.name != e.detail.name
		);
	}

	showPersonList() {
		// Oculta el formulario de alta y vuelve a mostrar la lista
		// Para ello maneja el shadow-root
		this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
		if (this.showPersonForm === true) {
			this.shadowRoot.getElementById("personForm").classList.add("d-none");
		}
	}

	showPersonFormData() {
		// Muestra el formularo de alta y oculta la listra
		// Para ello maneja el shadow-root
		if (this.showPersonForm === true) {
			this.shadowRoot.getElementById("personForm").classList.remove("d-none");
		}
		this.shadowRoot.getElementById("peopleList").classList.add("d-none");
	}

	personFormClose() {
		// Cierra el formulario, al cambiar la propiedad se despacha el evento updated
		this.showPersonForm = false;
	}

	personFormStore(e) {
		// Inserta una persona en el array, y muestra la lista (mediante el evento update del showPersonForm)
		this.people.push(e.detail.person);
		this.showPersonForm = false;
	}
	peopleDataUpdated(e) {
		console.log("Evento de actualizacion disparado");
		this.people = e.detail.people;
	}

}




customElements.define('persona-main', PersonaMain)