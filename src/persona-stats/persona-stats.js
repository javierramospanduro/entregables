import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: { type: Array }
        };
    }

    constructor() {
        super();
        console.log("console-stats - constructor");
    }

    updated(changedProperties) {
        console.log("people-stats -> changedProperties");
        if (changedProperties.has("people")) {
            console.log("people-stats -> changeg people")
            let peopleStats = this.gatherPeople(this.people);
            this.dispatchEvent(new CustomEvent("gather-people",{detail:{peopleStats:peopleStats}}));
        }
    }
    gatherPeople(people) {
        var peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;
    }
}

customElements.define('persona-stats', PersonaStats)